# Build environment
FROM alpine as builder

RUN apk add --update \
	build-base \
	autoconf \
        automake \
	texinfo \
        libtool \
	git \
    && rm -rf /var/cache/apk/*

RUN git clone https://github.com/hyperrealm/talkfilters.git

WORKDIR /talkfilters
RUN autoreconf --install \
	&& autoconf \
	&& mkdir build
	
WORKDIR /talkfilters/build
RUN ../configure --disable-dependency-tracking \
	&& make

RUN rm config.status libtool \
	&& mkdir /out \
	&& find . -perm /u=x -maxdepth 1 -type f -exec cp {} /out \;


# The actual container
FROM alpine

# We need the following:
# - fcgiwrap, because that is how nginx does CGI
# - spawn-fcgi, to launch fcgiwrap and to create the unix socket
# - nginx, because it is our frontend
RUN apk add --update nginx && \
    apk add --update fcgiwrap && \
    apk add --update spawn-fcgi && \
    rm -rf /var/cache/apk/*

COPY --from=builder /out /talkfilters
COPY nginx.conf /etc/nginx/nginx.conf
COPY ./*.sh /filter/


# launch fcgiwrap via spawn-fcgi; launch nginx in the foreground
# so the container doesn't die on us; supposedly we should be
# using supervisord or something like that instead, but this
# will do
CMD spawn-fcgi -s /run/fcgi.sock /usr/bin/fcgiwrap -f && \
    nginx -g "daemon off;"

