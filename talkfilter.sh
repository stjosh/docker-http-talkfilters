#!/bin/sh

if test -f /talkfilters/$FILTER; then
  echo "Status: 200"
  echo "Content-type: text/plain"
  echo ""
  echo `echo $BODY | /talkfilters/$FILTER`
else
  echo "Status: 404"
  echo ""
  echo "NOT FOUND"
fi


