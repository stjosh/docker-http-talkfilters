# docker-http-talkfilters
A Docker image to filter user input through [GNU talkfilters](https://github.com/hyperrealm/talkfilters).

**WARNING: EXPERIMENTAL!** The CGI scripts do not perform any input sanitization whatsoever. Use at your own risk and do not run on publicly available web servers.

Sample Usage:

```bash
$ docker run -d -p 8000:80 registry.gitlab.com/stjosh/docker-http-talkfilters:latest
```

To list the available filters, `GET /list`:

```bash
$ curl localhost:8000/list
austro b1ff brooklyn chef cockney drawl dubya fudd funetak jethro jive kraut pansy pirate postmodern redneck valspeak warez wrap
```

To filter something with the `pirate` filter, simply submit it as a `POST` request body:
```bash
$ curl -d "Hello, coworkers!" -X POST localhost:8000/filter/pirate
Avast, shipmates!
```
